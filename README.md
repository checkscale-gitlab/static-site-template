# Static Site Terraformed

This repo is a boilerplate to build out a static site hosted in S3 and also in CloudFront for full security and global availability.

## Resources

- S3 bucket to host site
- S3 bucket for `www` redirect
- ACM certificate
- Cloudfront distribution
- IAM user, access key, policy

## Inputs

- Route 53 Zone ID to be used
- Domain name (i.e. `example.com` or `static.example.com`)
- Secret between Cloudfront and S3

## Outputs

- AWS access key and secret used to deploy S3 bucket


## Additional Notes

The certificate is created automatically by adding DNS entries to the Route 53 hosted zone. The script will wait up to two hours for the certificate to be issued. If your domain is not owned by Route 53, you may need to go to the Route 53 hosted zone, look at the NS record, and assign your domain those nameservers. If the script times out because this was not done rerunning terraform apply after making sure the nameservers are correct should allow the module to continue.


## Diagram

![AWS_Static_Site](AWS_Static_Site.png)